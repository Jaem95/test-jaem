﻿Partial Class Home
    Inherits System.Web.UI.Page
    Dim AD As New AD()
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ds As New Data.DataSet
        ds = AD.BringUsers()
        If ds.Tables(0).Rows.Count > 0 Then
            tblUsuarios.DataSource = ds.Tables("Users")
            tblUsuarios.DataBind()
        End If

    End Sub
    Protected Sub Tabla1_SelectedIndexChanging(sender As Object, e As GridViewSelectEventArgs) Handles tblUsuarios.SelectedIndexChanging

        Dim index As Integer = e.NewSelectedIndex

        Dim user As String = tblUsuarios.Rows(index).Cells(0).Text
        Dim data As Boolean

        data = AD.UpdateInfo(user, "SI")

        If data Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Success!',  'Correct update!',  'success');grid.Refresh();</script>", False)
            UpdateTable()

        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Error!',  'Please contact to the administrador due an error!',  'error');grid.Refresh();</script>", False)
        End If


    End Sub

    Protected Sub Tabla1_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles tblUsuarios.RowDeleting

        Dim index As Integer = e.RowIndex

        Dim user As String = tblUsuarios.Rows(index).Cells(0).Text
        Dim data As Boolean

        data = AD.UpdateInfo(user, "NO")

        If data Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Success!',  'Correct update!',  'success');</script>", False)
            UpdateTable()
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Error!',  'Please contact to the administrador due an error!',  'error');grid.Refresh();</script>", False)
        End If

    End Sub

    Protected Sub UpdateTable()

        Dim ds As New Data.DataSet
        ds = AD.BringUsers()
        tblUsuarios.DataSource = ds.Tables(0)
        If ds.Tables(0).Rows.Count > 0 Then
            tblUsuarios.DataSource = ds.Tables("Users")
            tblUsuarios.DataBind()
        Else
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Error!',  'Theres no users!',  'error');grid.Refresh();</script>", False)

        End If

    End Sub



    Protected Sub Unnamed_Click(sender As Object, e As EventArgs)
        Page.Validate("1")
        If Page.IsValid Then
            Dim validacion As Boolean = AD.ValidateUser(txtUserName.Text)
            If validacion Then
                ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'>swal(  'Error!',  'The user name that you select  already exits in the app!',  'error');</script>", False)
            Else
                validacion = AD.RegisterUsers(nombre.Text, apaterno.Text, amaterno.Text, txtUserName.Text, txtPass.Text, txtAddress.Text, txtCity.Text, ddlStates.SelectedValue, txtNumber.Text)
                If validacion Then
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), "temp", "<script language='javascript'> swal({title: 'Success!',text: 'The user was correctly registered!',type: 'success',confirmButtonColor: '#3085d6',cancelButtonColor: '#d33',confirmButtonText: 'OK'}).then(function () { location.reload() })  ;$('#myModal').modal('hide');</script>", False)
                End If
            End If
        End If

    End Sub
    Protected Sub btnLogOut_Click(sender As Object, e As EventArgs)
        Response.Redirect("Default.asp")
    End Sub
End Class
