﻿$("#loginForm").validate({
    rules: {
        username: {
            required: true
        },
        password: {
            required: true
        }
    },
    messages: {
        username: {
            required: "Please enter  your username"
        },
        password: {
            required: "Please enter your password"
        }
    },
    submitHandler: function (form) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function (response) {
                if (response == 1) {
                    window.location = "Home.aspx"
                }
                else {
                    swal(
                      'Fail!',
                      'The  username or the password are incorrect!',
                      'error'
                    )
                }
            }
        });
    }
});
$('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function () {
    $(this).removeClass('input-error');
});

$('.login-form').on('submit', function (e) {

    $(this).find('input[type="text"], input[type="password"], textarea').each(function () {
        if ($(this).val() == "") {
            e.preventDefault();
            $(this).addClass('input-error');
        }
        else {
            $(this).removeClass('input-error');
        }
    });

});



jQuery(document).ready(function () {
    $.backstretch("assets/img/backgrounds/1.jpg");
});

