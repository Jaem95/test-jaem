﻿Public Class AD
    Public Function BringUsers() As System.Data.DataSet
        Dim ds As New System.Data.DataSet
        Dim da As New System.Data.SqlClient.SqlDataAdapter
        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "select * from tbl_usuarios order by fecha_registro desc"
        Dim connexio As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(myConnectionString)
        Dim myCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            myCommand.CommandText = query
            myCommand.ExecuteNonQuery()
            da.SelectCommand = myCommand
            da.Fill(ds, "Users")


            ds.Tables(0).Columns(0).ColumnName = "id_usuario"
            ds.Tables(0).Columns(1).ColumnName = "user_name"
            ds.Tables(0).Columns(2).ColumnName = "pass"
            ds.Tables(0).Columns(3).ColumnName = "nombre"
            ds.Tables(0).Columns(4).ColumnName = "apaterno"
            ds.Tables(0).Columns(5).ColumnName = "amaterno"
            ds.Tables(0).Columns(6).ColumnName = "telefono"
            ds.Tables(0).Columns(7).ColumnName = "calle"
            ds.Tables(0).Columns(8).ColumnName = "ciudad"
            ds.Tables(0).Columns(9).ColumnName = "id_estado"
            ds.Tables(0).Columns(10).ColumnName = "fecha_registro"

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try


        Return ds
    End Function

    Public Function UpdateInfo(user As String, status As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "update tbl_usuarios set activo = '" & status & "' where id_usuario = " & user & ""
        Dim connexio As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(myConnectionString)
        Dim myCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If


        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion


    End Function

    Public Function ValidateUser(user As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "select * from tbl_usuarios where user_name = '" & user & "'"
        Dim connexio As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(myConnectionString)
        Dim myCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteScalar()
            If rowsAffected > 0 Then
                validacion = True
            End If

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion


    End Function

    Public Function RegisterUsers(nombre As String, apaterno As String, amaterno As String, txtUserName As String, txtPass As String, txtAddress As String, txtCity As String, ddlStates As String, txtNumber As String) As Boolean
        Dim validacion As Boolean = False
        Dim rowsAffected As Integer = 0

        Dim myConnectionString As String = ConfigurationManager.ConnectionStrings("DB").ToString()
        Dim query As String = "insert into tbl_usuarios values('" & txtUserName & "','" & txtPass & "','" & nombre & "','" & apaterno & "','" & amaterno & "','" & txtNumber & "','" & txtAddress & "','" & txtCity & "'," & ddlStates & ",'SI',SYSDATETIME ( ))"
        Dim connexio As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(myConnectionString)
        Dim myCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(query)
        myCommand.Connection = connexio


        Try
            connexio.Open()
            rowsAffected = myCommand.ExecuteNonQuery()
            If rowsAffected > 0 Then
                validacion = True

            End If

        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine("Error en la conexion a la BD")

        Finally
            connexio.Close()
        End Try

        Return validacion

    End Function




End Class
