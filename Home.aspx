﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Home.aspx.vb" Inherits="Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
    <%-- <link rel="stylesheet" href="assets/css/form-elements.css">--%>
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/css/sweetalert2.min.css" rel="stylesheet" />

    <!-- Javascript -->
    <script src="assets/js/jquery-1.11.1.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/jquery.backstretch.min.js"></script>
    <script src="assets/js/sweetalert2.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server"></asp:ScriptManager>
        <div>
            <div class="top-content">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>App Demo</strong></h1>
                            <div class="description">
                                <p>
                                    This app was made by JAEM with the purpose of join different tools of web app development and  show the skills of the developer
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-11 col-md-push-1 col-sm-12 col-xs-12 form-box">
                            <div class="form-top">
                                <div class="form-top-left">
                                    <h3>User Managament</h3>

                                </div>
                                <div class="form-top-right">
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>
                            <div class="form-bottom">
                                <table class="table table-responsive table-bordered table-striped table-hover">

                                    <asp:GridView ID="tblUsuarios" runat="server" class="table table-responsive table-bordered table-striped" AutoGenerateColumns="false" ClientInstanceName="grid">
                                        <Columns>
                                            <asp:BoundField DataField="id_usuario" HeaderStyle-HorizontalAlign="Center" HeaderText="ID User" />
                                            <asp:BoundField DataField="nombre" HeaderStyle-HorizontalAlign="Center" HeaderText="Name" />
                                            <asp:BoundField DataField="apaterno" HeaderStyle-HorizontalAlign="Center" HeaderText="Surname" />
                                            <asp:BoundField DataField="amaterno" HeaderStyle-HorizontalAlign="Center" HeaderText="Surname" />
                                            <asp:BoundField DataField="calle" HeaderStyle-HorizontalAlign="Center" HeaderText="Address" />
                                            <asp:BoundField DataField="ciudad" HeaderStyle-HorizontalAlign="Center" HeaderText="City" />
                                            <asp:BoundField DataField="telefono" HeaderStyle-HorizontalAlign="Center" HeaderText="Phone" />
                                            <asp:BoundField DataField="activo" HeaderStyle-HorizontalAlign="Center" HeaderText="Available" />
                                            <asp:CommandField DeleteText="Disable" ShowDeleteButton="True" />
                                            <asp:CommandField DeleteText="" SelectText="Active" ShowSelectButton="True" />

                                        </Columns>
                                    </asp:GridView>

                                </table>


                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 15px">
                        <div class="col-md-12 form-group">
                            <div class="col-md-3 col-md-offset-3">
                                <input type="button" class="form-control btn  btn-success " value="Register New User" data-toggle="modal" data-target="#myModal" />
                            </div>
                            <div class="col-md-3">
                                <asp:Button runat="server" ID="btnLogOut" Text="Sign out" CssClass="form-control btn  btn-danger" OnClick="btnLogOut_Click" />
                            </div>

                        </div>

                    </div>

                </div>
            </div>

        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Register New User</h4>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel runat="server">
                            <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-12 form-group">
                                        <div class="col-md-4">
                                            <label for="nombre">Name:</label>
                                            <asp:TextBox runat="server" placeholder="Name" CssClass="form-control" ID="nombre"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="nombre" ErrorMessage="Please fill your name" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>

                                        </div>
                                        <div class="col-md-4">
                                            <label for="nombre">Surname:</label>
                                            <asp:TextBox runat="server" placeholder="First Surname" CssClass="form-control" ID="apaterno"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="apaterno" ErrorMessage="Please fill your first surname" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nombre">Surname:</label>
                                            <asp:TextBox runat="server" placeholder="Second Name" CssClass="form-control" ID="amaterno"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="amaterno" ErrorMessage="Please fill your second surname" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        

                                        </div>
                                        <div class="col-md-6">
                                            <label for="nombre">User Name:</label>
                                            <asp:TextBox runat="server" placeholder="User name" CssClass="form-control" ID="txtUserName"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUserName" ErrorMessage="Please select your username" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        
                                        </div>
                                        <div class="col-md-6">
                                            <label for="nombre">Password:</label>
                                            <asp:TextBox runat="server" placeholder="Password" CssClass="form-control" ID="txtPass" TextMode="Password"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPass" ErrorMessage="Please  select a password" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nombre">Address:</label>
                                            <asp:TextBox runat="server" placeholder="Address" CssClass="form-control" ID="txtAddress"></asp:TextBox>
                                             <asp:RequiredFieldValidator runat="server" ControlToValidate="txtAddress" ErrorMessage="Please type your address" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nombre">City:</label>
                                            <asp:TextBox runat="server" placeholder="City" CssClass="form-control" ID="txtCity"></asp:TextBox>
                                             <asp:RequiredFieldValidator runat="server" ControlToValidate="txtCity" ErrorMessage="Please type your city" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        
                                        </div>
                                        <div class="col-md-4">
                                            <label for="nombre">State:</label>

                                            <asp:DropDownList runat="server" CssClass="form-control" ID="ddlStates" DataSourceID="cat_estados" DataTextField="estado" DataValueField="id_estado">
                                            </asp:DropDownList>

                                            <asp:SqlDataSource runat="server" ID="cat_estados" ConnectionString='<%$ ConnectionStrings:DB %>' SelectCommand="SELECT * FROM [cat_estados]"></asp:SqlDataSource>
                                        </div>
                                        <div class="col-md-12">
                                            <label for="nombre">Phone Number:</label>
                                            <asp:TextBox runat="server" placeholder="Phone number" CssClass="form-control" ID="txtNumber"></asp:TextBox>
                                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtNumber" ErrorMessage="Please type your phone number" ForeColor="Red" ValidationGroup="1"></asp:RequiredFieldValidator>
                                        
                                        </div>
                                        <div class="col-md-12" style="margin-top:10px">
                                            <asp:Button runat="server" CssClass="form-control btn btn-info btn-lg" Text="Register"  OnClick="Unnamed_Click"/>

                                        </div>

                                    </div>

                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </form>


    <script src="assets/js/custom.js"></script>
</body>
</html>
